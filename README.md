# Reign Fullstack Challenge

Hello there! Before starting, I just want to thank you guys for the opportunity and for taking the time to evaluate my the test.

This is my first time developing with nest.js and typescript, so I struggled a bit at beginning. I can understand if you consider I made some mistakes in the code or ignored anything.

## Start the app

To run the app, just do `docker-compose up -d` and it shall pull the mongodb image, and build server and client respectively. You will be able to
test the app from your browser on `localhost:3000`.

## Run Tests

I only made tests for the server component, as the doc said, so if you wanna run them from the outside (with docker-compose), just do `docker-compose exec server npm run test`, otherwise just `cd server/` and install dependencies with `npm install` and finally just do `npm run test`

### Annotations

I couldn't to the extra considerations part, as I have absolutely no experience on that, and taking the time to understand the process and run all the things needed will take more time than the expected, so I apologize for that.
