import React from "react";

// View components
import Home from "./views/Home";

// Context
import { GlobalProvider } from "./context/global";

const App = () => {
  return (
    <GlobalProvider>
      <Home />
    </GlobalProvider>
  );
};

export default App;
