import React from "react";

// Styled components
import { StyledButton } from "./styles";

const Button = (props) => {
  return (
    <StyledButton onClick={props.onClick} success={props.success}>
      {props.text}
    </StyledButton>
  );
};

Button.defaultProps = {
  text: `Button`,
  success: true,
  onClick: () => {},
};

export default Button;
