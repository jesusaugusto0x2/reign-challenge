import styled from "styled-components";

export const StyledButton = styled.button`
  border: none;
  border-radius: 5px;
  color: white;
  cursor: pointer;
  font-size: 16px;
  padding: 4px 16px;
  background-color: ${(props) => (props.success ? "#5ce16a" : "#ff4e4e;")};

  &:hover {
    background-color: ${(props) => (props.success ? "#53cc60;" : "#e63131")};
  }
`;
