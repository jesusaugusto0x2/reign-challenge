import React from "react";

// Styled Components
import { TitleSection } from "./styles";

const Header = () => {
  return (
    <TitleSection>
      <p>{`HN Feed`}</p>
      <p>{`We <3 hacker news!`}</p>
    </TitleSection>
  );
};

export default Header;
