import styled from "styled-components";

export const TitleSection = styled.header`
  color: white;
  background: #333;
  padding: 60px 35px;

  p {
    margin: 0;
    font-size: 1.6rem;

    &:first-child {
      font-size: 5rem;
      font-weight: bold;
    }

    &:nth-child(2) {
      margin-top: 8px;
    }
  }
`;
