import React, { useState } from "react";

// Custom components
import Button from "components/Button";

// Styled components
import { FullWrapper, Content, ButtonsContent } from "./styles";

// Context
import { useGlobalContext } from "context/global";

// Hooks
import { useDeleteRequest } from "hooks/useHttpRequests";

const Modal = () => {
  const [isOverModal, setIsOverModal] = useState(false);

  const { state, dispatch } = useGlobalContext();

  const [execute] = useDeleteRequest(`http://localhost:8000/api/post/delete`);

  const closeModal = () => {
    !isOverModal &&
      dispatch({
        type: "HANDLE_DELETE_MODAL",
        payload: { isModalOpen: false, noticeId: null },
      });
  };

  const deleteAndclose = async () => {
    await execute(state.noticeId);

    dispatch({
      type: "DELETE_NEWS_AND_CLOSE_MODAL",
      payload: { isModalOpen: false, noticeId: null, reload: !state.reload },
    });
  };

  return (
    <>
      {state.isModalOpen && (
        <FullWrapper onClick={closeModal}>
          <Content
            onMouseEnter={() => setIsOverModal(true)}
            onMouseLeave={() => setIsOverModal(false)}
          >
            <h2>{`Are you sure to delete the news?`}</h2>
            <h4>{`Once you delete it, you may not see it again!`}</h4>
            <ButtonsContent
              onMouseEnter={() => setIsOverModal(!isOverModal)}
              onMouseLeave={() => setIsOverModal(!isOverModal)}
            >
              <Button success={false} onClick={closeModal} text={`No`} />
              <Button onClick={deleteAndclose} text={`Yes`} />
            </ButtonsContent>
          </Content>
        </FullWrapper>
      )}
    </>
  );
};

export default Modal;
