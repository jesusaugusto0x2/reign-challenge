import styled from "styled-components";

export const FullWrapper = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.2);
`;

export const Content = styled.div`
  position: fixed;
  padding: 20px;
  background-color: rgba(253, 253, 253, 1);
  width: 500px;
  margin: 5% auto;
  left: 0;
  right: 0;
  border-radius: 5px;
  display: flex;
  flex-direction: column;
  align-content: center;
  align-items: center;

  h2,
  h4 {
    font-weight: normal;
  }
`;

export const ButtonsContent = styled.div`
  display: flex;
  justify-content: space-between;
  width: 25%;
`;
