import React, { useState } from "react";
import PropTypes from "prop-types";

// Styled components
import {
  Wrapper,
  ContentSection,
  Title,
  Author,
  TrashCan,
  Delete,
} from "./styles";

// Context
import { useGlobalContext } from "context/global";

// Custom utilities
import { parseDayDifference } from "utils/dateParser";
import { openInNewTab } from "utils/redirection";

const Notice = (props) => {
  const [isHovered, setIsHovered] = useState(false);
  const [isTrashHovered, setIsTrashHovered] = useState(false);

  const { dispatch } = useGlobalContext();

  const clickTrashCan = () => {
    dispatch({
      type: "HANDLE_DELETE_MODAL",
      payload: { isModalOpen: true, noticeId: props.id },
    });
  };

  return (
    <Wrapper
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
      isHovered={isHovered}
      onClick={() => openInNewTab(props.url, !isTrashHovered)}
    >
      <ContentSection>
        <Title>{props.title}</Title>
        <Author>{`- ${props.author} -`}</Author>
      </ContentSection>

      <ContentSection>
        <span>{parseDayDifference(props.date)}</span>
        <Delete>
          {isHovered && (
            <TrashCan
              onMouseEnter={() => setIsTrashHovered(true)}
              onMouseLeave={() => setIsTrashHovered(false)}
              onClick={clickTrashCan}
            />
          )}
        </Delete>
      </ContentSection>
    </Wrapper>
  );
};

Notice.defaultProps = {
  id: "1",
  title: "WordPress 4.3 will be rewritten in Node.js",
  author: "Garbarge",
  date: `Today`,
  url: null,
};

Notice.propTypes = {
  id: PropTypes.string,
  title: PropTypes.string,
  author: PropTypes.string,
  date: PropTypes.string,
  url: PropTypes.string,
};

export default Notice;
