import styled from "styled-components";

import { ReactComponent as TrashLogo } from "../../assets/delete.svg";

export const Wrapper = styled.div`
  padding: 20px;
  border-bottom: 1px solid #cccccc;
  background-color: "#fff";
  display: flex;
  justify-content: space-between;
  &:hover {
    cursor: pointer;
    background-color: #fafafa;
  }
`;

export const ContentSection = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const Title = styled.span`
  font-weight: bold;
  margin-right: 12px;
`;

export const Author = styled.span`
  color: #999;
`;

export const TrashCan = styled(TrashLogo)`
  width: 25px;
  height: 25px;
  margin-left: 12px;
`;

export const Delete = styled.div`
  width: 100px;
  height: 30px;
  display: flex;
  justify-content: flex-end;
  background-color: transparent;
`;
