import React, { useReducer, useContext } from "react";

const initialState = {
  noticeId: null,
  isModalOpen: false,
  reload: false,
};

const reducer = (state, action) => {
  switch (action.type) {
    case "HANDLE_DELETE_MODAL": {
      const { isModalOpen, noticeId } = action.payload;

      return {
        ...state,
        isModalOpen: isModalOpen,
        noticeId: noticeId,
      };
    }

    case "DELETE_NEWS_AND_CLOSE_MODAL": {
      const { isModalOpen, noticeId, reload } = action.payload;

      return {
        ...state,
        isModalOpen: isModalOpen,
        noticeId: noticeId,
        reload: reload,
      };
    }

    default:
      break;
  }

  return state;
};

const GlobalContext = React.createContext();

const GlobalProvider = (props) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <GlobalContext.Provider
      value={{
        state: state,
        dispatch: dispatch,
      }}
    >
      {props.children}
    </GlobalContext.Provider>
  );
};

export { GlobalProvider, GlobalContext };

// Return the provider as a hook
export const useGlobalContext = () => useContext(GlobalContext);
