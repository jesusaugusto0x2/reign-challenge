import { useState, useEffect } from "react";
import axios from "axios";

// Context
import { useGlobalContext } from "context/global";

export const useGetRequest = (url) => {
  const [data, setData] = useState([]);

  const { state } = useGlobalContext();

  useEffect(() => {
    const fetchData = async () => {
      const resp = await axios.get(url).then((response) => {
        return response.data;
      });

      setData(resp);
    };

    fetchData();
  }, [url, state.reload]);

  return [data];
};

export const useDeleteRequest = (url) => {
  const execute = async (id) => {
    console.log("deleting id: ", id);
    return axios.delete(`${url}/${id}`);
  };

  return [execute];
};
