import { isToday, format, isYesterday } from "date-fns";

export const parseDayDifference = (_date) => {
  const date = new Date(_date);

  if (isToday(date)) {
    return `${format(date, "H:mm aaa")}`;
  } else if (isYesterday(date)) {
    return "Yesterday";
  }

  return format(date, "MMM d");
};
