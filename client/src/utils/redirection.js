export const openInNewTab = (url, canOpen = true) => {
  if (canOpen) {
    const newWindow = window.open(url, "_blank", "noopener,noreferrer");
    if (newWindow) {
      newWindow.opener = null;
    }
  }
};
