import React from "react";

// Custom components
import Header from "components/Header";
import Notice from "components/Notice";
import Modal from "components/Modal";

// Styled components
import { Feed } from "./styles";

// Custom hooks
import { useGetRequest } from "hooks/useHttpRequests";

const Home = () => {
  const [data] = useGetRequest(`http://localhost:8000/api/post`);

  return (
    <div style={{ position: "relative" }}>
      <Modal />
      <Header />
      <Feed>
        {data.news &&
          data.news.map((notice) => {
            return (
              <Notice
                key={notice._id}
                id={notice._id}
                title={notice.story_title || notice.title}
                author={notice.author}
                date={notice.created_at}
                url={notice.story_url || notice.url}
              />
            );
          })}
      </Feed>
    </div>
  );
};

export default Home;
