import styled from "styled-components";

export const Feed = styled.div`
  display: flex;
  flex-direction: column;
  padding: 12px 30px;
`;
