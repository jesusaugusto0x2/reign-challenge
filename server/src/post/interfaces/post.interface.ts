import { Document } from 'mongoose';

export interface Post extends Document {
  readonly _id: string;
  readonly title: string;
  readonly story_title: string;
  readonly url: string;
  readonly story_url: string;
  readonly author: string;
  readonly created_at: Date;
  readonly deleted_at: Date;
}
