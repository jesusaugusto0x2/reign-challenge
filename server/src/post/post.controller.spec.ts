import { HttpModule } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { PostController } from './post.controller';
import { getModelToken } from '@nestjs/mongoose';
import { PostService } from './post.service';
import { Post } from './schemas/post.schema';

describe('PostController', () => {
  let controller: PostController;

  beforeEach(async () => {
    // Mocking the post model functions
    function mockPostModel(entity: any) {
      this.find = jest.fn((entity) => entity);
      this.findOne = jest.fn((entity) => (entity.clientId === 'exists' ? null : entity));
      this.sort = jest.fn((entity) => entity);
      this.save = jest.fn((entity) => entity);
      this.findById = jest.fn((entity) => (entity.clientId === 'exists' ? null : entity));
      this.findByIdAndUpdate = jest.fn((entity) => (entity) => entity);
    }

    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [PostController],
      providers: [PostService, { provide: getModelToken(Post.name), useValue: mockPostModel }],
    }).compile();

    controller = module.get<PostController>(PostController);
  });

  it('Should be defined', () => {
    expect(controller).toBeDefined();
  });
});
