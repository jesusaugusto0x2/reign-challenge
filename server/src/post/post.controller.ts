import { Controller, Get, Delete, Res, Param, HttpStatus } from '@nestjs/common';

import { PostService } from './post.service';

@Controller('post')
export class PostController {
  constructor(private postService: PostService) {}

  @Get('/')
  async getPosts(@Res() res) {
    const resp = await this.postService.getPosts();

    return res.status(HttpStatus.OK).json({
      news: resp,
    });
  }

  @Get('/find')
  async findNews() {
    await this.postService.findPosts();

    return 'Parsed';
  }

  @Delete('/delete/:postID')
  async deletePost(@Param('postID') postID, @Res() res) {
    const deleted = await this.postService.deletePost(postID);

    if (!deleted) {
      return res.status(HttpStatus.NOT_FOUND).json({
        message: 'Post not found',
      });
    }

    return res.status(HttpStatus.OK).json({
      message: 'Post successfully deleted',
      post: deleted,
    });
  }
}
