import { HttpModule } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { PostService } from './post.service';
import { Post } from './schemas/post.schema';

describe('PostService', () => {
  let service: PostService;

  beforeEach(async () => {
    // Mocking the post model functions
    function mockPostModel(entity: any) {
      this.find = jest.fn((entity) => entity);
      this.findOne = jest.fn((entity) => (entity.clientId === 'exists' ? null : entity));
      this.sort = jest.fn((entity) => entity);
      this.save = jest.fn((entity) => entity);
      this.findById = jest.fn((entity) => (entity.clientId === 'exists' ? null : entity));
      this.findByIdAndUpdate = jest.fn((entity) => (entity) => entity);
    }

    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [PostService, { provide: getModelToken(Post.name), useValue: mockPostModel }],
    }).compile();

    service = module.get<PostService>(PostService);
  });

  it('Should be defined', () => {
    expect(service).toBeDefined();
  });

  it('Should return an array of Posts', async () => {
    const result = [
      {
        _id: 'string',
        title: 'string',
        story_title: 'string',
        url: 'string',
        story_url: 'string',
        author: 'string',
        created_at: new Date(),
        deleted_at: null,
      },
    ] as Post[];

    jest.spyOn(service, 'getPosts').mockImplementation(async (): Promise<Post[]> => Promise.resolve(result));

    expect(await service.getPosts()).toBe(result);
  });

  it('Should delete a post', async () => {
    const result = {
      deleted_at: '2021-06-09T00:45:34.256Z',
      _id: '60c00ca0102fea5a1f821b0a',
    } as any as Post;

    jest.spyOn(service, 'deletePost').mockImplementation(async (): Promise<Post> => Promise.resolve(result));

    expect(await service.deletePost('any_id')).toBe(result);
  });

  it('Should not insert when previous existent data', async () => {
    const externalData = {
      clientId: 'exists',
      title: 'title',
      story_title: 'story_title',
    };

    jest.spyOn(service, 'validateInsertion').mockImplementation(async () => null);

    expect(await service.validateInsertion(externalData)).toBe(null);
  });

  it('Should insert a new news item', async () => {
    const externalData = {
      clientId: 'entity_id',
      title: 'title',
      story_title: 'story_title',
    };

    jest.spyOn(service, 'validateInsertion').mockImplementation(async () => Promise.resolve({ existent: true }));

    expect(await service.validateInsertion(externalData)).toStrictEqual({ existent: true });
  });
});
