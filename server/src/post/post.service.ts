import { Injectable, HttpService, Logger, OnModuleInit } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Post, PostDocument } from './schemas/post.schema';

@Injectable()
export class PostService implements OnModuleInit {
  private readonly logger = new Logger(PostService.name);

  constructor(@InjectModel(Post.name) readonly postModel: Model<PostDocument>, private httpService: HttpService) {}

  async onModuleInit(): Promise<void> {
    this.logger.debug('Initializing post service');
    await this.findPosts();
  }

  async getPosts(): Promise<Post[]> {
    const posts = await this.postModel
      .find({
        deleted_at: {
          $eq: null,
        },
      })
      .sort({
        created_at: 'desc',
      });

    return posts;
  }

  async deletePost(postID: string): Promise<Post> {
    return this.postModel.findByIdAndUpdate(postID, {
      deleted_at: new Date(),
    });
  }

  @Cron(CronExpression.EVERY_HOUR)
  async findPosts() {
    this.logger.debug('Finding new posts!');

    const resp = await this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs').toPromise();

    resp.data.hits.forEach(async (post) => {
      const postData = {
        _id: post.objectID,
        title: post.title,
        story_title: post.story_title,
        url: post.url,
        story_url: post.story_url,
        created_at: post.created_at,
        author: post.author,
        deleted_at: null,
      };

      await this.validateInsertion(postData);
    });
  }

  async validateInsertion(postData: any): Promise<any> {
    const existent = await this.postModel.findById(postData._id);

    const isDeleted = existent ? existent.deleted_at : null;

    if ((postData.story_title || postData.title) && !isDeleted) {
      const newPost = !existent && (await new this.postModel(postData));

      newPost && (await newPost.save());

      return newPost;
    }

    return null;
  }
}
