import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PostDocument = Post & Document;

@Schema()
export class Post {
  @Prop()
  _id: string;

  @Prop()
  title: string;

  @Prop()
  story_title: string;

  @Prop()
  url: string;

  @Prop()
  story_url: string;

  @Prop()
  author: string;

  @Prop()
  created_at: Date;

  @Prop({ default: null })
  deleted_at: Date;
}

export const PostSchema = SchemaFactory.createForClass(Post);
